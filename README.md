#Module 2 - Individual

Files:

calculator.html

  Front end for calculator module. Holds the buttons and fields for user to enter data.

calculator_back.PHP

  Back end for calculator module. Holds the logic for all four operations and checks edge cases.

link to calculator: http://ec2-18-217-56-56.us-east-2.compute.amazonaws.com/~johnf/spring2018-module2-individual-451200/calculator.html



link to userDir folder:
http://ec2-18-217-56-56.us-east-2.compute.amazonaws.com/~johnf/apache_userDir.txt
