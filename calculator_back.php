<?php
        $num1 = $_GET['num1'];
        $num2 = $_GET['num2'];

        $operation = $_GET['operators'];

        $result = 0;

        if(is_numeric($num1) && is_numeric($num2)) {
          if($operation == "add") {
            $result = $num1 + $num2;
            echo ($num1 . " + " . $num2 . " = " . $result);
          }

          if($operation == "subtract") {
            $result = $num1 - $num2;
            echo ($num1 . " – " . $num2 . " = " . $result);
          }

          if($operation == "product") {
            $result = $num1 * $num2;
            echo ($num1 . " x " . $num2 . " = " . $result);
          }

          if($operation == "divide") {
            if($num2 == 0) {
              echo "Divide by zero error";
            }
            if($num2 != 0) {
              $result = $num1 / $num2;
              echo ($num1 . " ÷ " . $num2 . " = " . $result);
            }
          }
        }
        else {
          echo "Invalid Entry";
        }

       ?>
